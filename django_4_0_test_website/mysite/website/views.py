from django.http import HttpRequest, HttpResponse
from  django.contrib.messages import add_message, SUCCESS
from django.shortcuts import redirect, render

from .models import Contact

def index(request: HttpRequest) -> HttpResponse:
    return render(request=request, template_name='website/index.html', context={})

def contact(request: HttpRequest) -> HttpResponse:
    if request.method == 'POST':
        name = request.POST.get('name', '')
        email = request.POST.get('email', '')
        title = request.POST.get('title', '')
        message = request.POST.get('message', '')
        contact = Contact()
        contact.name = name
        contact.email = email
        contact.title = title
        contact.message = message
        contact.save(True)
        add_message(request, message='Thank you for your message', level=SUCCESS, extra_tags='success')
        return redirect('/')
        return render(request, 'website/thank_you.html', {'is_thank_you': True})
    return render(request=request, template_name='website/contact.html', context={})

def about(request: HttpRequest) -> HttpResponse:
    return render(request=request, template_name='website/about.html', context={})
