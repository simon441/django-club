from typing import List
from django import shortcuts, template
from django.urls import NoReverseMatch
from django.utils import safestring
register = template.Library()


@register.simple_tag(takes_context=True, name='active_link')
def active_link(context: template.RequestContext, current_page, class_list=None, active_class_name='active', *args, **kwargs) -> str:
    """Display active link

    Args:
        context (template.RequestContext): The Request context
        current_page (_type_): The view to compare to
        class_list (_type_, optional): The classes to apply to the link tag (a). Defaults to None.
        active_class_name (str, optional): active class name. Defaults to 'active'.

    Returns:
        str: class="" aria-current="page" if current page  else class=""
    """
    request = context.request
    if request is None:
        return ''
    current_url = request.path
    try:
        current_page = shortcuts.resolve_url(current_page)
    except NoReverseMatch as e:
        return ''
    
    if type(class_list) is List:
        class_list = ' ' .join(class_list)

    # build class and aria attribute for current page
    text = f' class="{class_list}'
    if current_page == current_url:
        text += f' {active_class_name}"  aria-current="page" '
    else:
        text += '" '

    return safestring.mark_safe(text)
