# Django 2021

by Codemy.com

playlist: []

1. Install Django And Build First Webpage - Django Wednesdays #1 [https://youtu.be/HHx3tTQWUx0]
2. How To Add A Calendar To Your Website - Django Wednesdays #2 [https://youtu.be/4EJlrweJE-M]
3. How To Use Templates and Custom HTML - Django Wednesdays #3 [https://youtu.be/NVeV7Y3BkF4]
4. Using Multiple Database Tables With Django - Django Wednesdays #4 [https://youtu.be/z5e_8FgKZig]
5. Fetch Data From a Database And Output To A Webpage - Django Wednesdays #5
6. How To Modify The Django Admin Area - Django Wednesdays #6
7. How To Add Database Forms To A Web Page - Django Wednesdays #7
8. How To Query The Database For Venues - Django Wednesdays #8
9. Create A Search Bar - Django Wednesdays #9
10. Update and Edit Venues - Django Wednesdays #10
11. Create An Add Event Page - Django Wednesdays #11
12. Create An Update Events Page - Django Wednesdays #12
13. Delete Items From The Database - Django Wednesdays #13
14. How To Order Items From The Database - Django Wednesdays #14
15. How To Generate Text Files Dynamically With Django - Django Wednesdays #15
16. How To Generate CSV Spreadsheet Files Dynamically With Django - Django Wednesdays #16
17. How To Generate PDF Files Dynamically With Django - Django Wednesdays #17
18. Pagination For Django - Django Wednesdays #18
19. Style Pagination With Bootstrap - Django Wednesdays #19
20. Customize Django Admin Titles - Django Wednesdays #20
21. Login With User Authentication - Django Wednesdays #21
22. Log Out With User Authentication - Django Wednesdays #22
23. Navbar Dropdown Menus - Django Wednesdays #23
24. How To Register Users - Django Wednesdays #24
25. Add Extra Registration Fields - Django Wednesdays #25
26. Style Django Registration Forms - Django Wednesdays #26
27. Lock Down Your Django App - Django Wednesdays #27
28. Determine Correct User To Edit Events - Django Wednesdays #28
29. Add Venue Owner - Django Wednesdays #29
30. Use Different Forms For Different Users - Django Wednesdays #30
31. Only Let Manger Delete Event - Django Wednesdays #31
32. Access Venue Owner - Django Wednesdays #32
33. Add My_Events Page - Django Wednesdays #33
34. Search Events - Django Wednesdays #34
35. Style Venue Search Results - Django Wednesdays #35
36. Query Filter Events By Date - Django Wednesdays #36
37. Django 4.0 Release Notes! - Django Wednesdays #37
38. Upload Images To Django - Django Wednesdays #38
39. Edit Uploaded Images - Django Wednesdays #39
40. How To Add Calculated Fields To Models - Django Wednesdays #40
41. Remove Groups From Admin Site - Django Wednesdays #41
42. Event Approval - Django Wednesdays #42
43. Build An Event Approval Page With Checkboxes - Django Wednesdays #43
44. Count The Number Of Events and Venues - Django Wednesdays #44
45. Show All Events By Venue - Django Wednesdays #45
46. Build A Website With Django 4.0! - Django Wednesdays #46

## To begin using the project

- create virtual env: ``` python -m venv ./venv ```
- activate virtual env: bash:``` source ./venv/Scripts/activate ```, windows: ```venv\Scripts\activate.bat```
- install dependencies: ``` pip install -r requirements.txt ```
- navigate to project: ``` cd myclub_webite ```
- migrate database: ``` python manage.py migrate ```
- create a superuser to log in the admin section: ``` python manage.py createsuperuser ```
- start web server: ``` python manage.py runserver ```
- to use the app, open your web browser and go to the url: [http://localhost:8000/]

## Deployment (WSGI)

using waitress
go to myclub_webite folder using `cd myclub_webite` then use the `app.sh` script. To visit the website go to `127.0.0.1:5000` or any other address with the port `5000`.
