from django.urls import path
from . import views

urlpatterns = [
    # Path converters
    # by default uses 'str' converter
    # int: numbers [0-9]+ (Matches zero or any positive integer)
    # str: strings ( Matches any non-empty string, excluding the path separator, '/')
    # path: complete urls (Matches any non-empty string, including the path separator, '/'.)
    # slug: hyphen-and_underscores_stuff [a-zA-Z0-9\-_] (ASCII letters or numbers, plus the hyphen and underscore characters)
    # UUID: Universally Unique IDentifier [a-z0-9\-](To prevent multiple URLs from mapping to the same page, dashes must be included and letters must be lowercase)
    # generic paths
    path('', views.home, name='events.home'),
    path('<int:year>/<str:month>/', views.home, name='events.home'),

    # events
    # CRUD
    path('events/', views.all_events, name='events.list'),
    path('add_event/', views.add_event, name='events.add'),
    path('update_event/<int:event_id>/',
         views.update_event, name='events.update'),
    path('delete_event/<int:event_id>/',
         views.delete_event, name='events.delete'),
    path('show_event/<int:event_id>/', views.show_event, name='events.show'),
    # generic route
    path('my_events/', views.my_events, name='events.my_events'),
    # search
    path('search_events/', views.search_events, name='events.search'),
    # Admin event approval
    path('admin_approval/', views.admin_approval, name='events.admin_approval'),

    # venues
    # CRUD
    path('add_venue/', views.add_venue, name='events.venues.add'),
    path('list_venues/', views.list_venues, name='events.venues.list'),
    path('show_venue/<int:venue_id>/',
         views.show_venue, name='events.venues.show'),
    path('update_venue/<int:venue_id>/',
         views.update_venue, name='events.venues.update'),
    path('delete_venue/<int:venue_id>/',
         views.delete_venue, name='events.venues.delete'),
    # search
    path('search_venue/', views.search_venues, name='events.venues.search'),
    # generate venue documents
    path('venue_text/', views.venue_text, name='venue.text'),
    path('venue_csv/', views.venue_csv, name='venue.csv'),
    path('venue_pdf/', views.venue_pdf, name='venue.pdf'),
    # Show venue  events
    path('venue_events/<int:venue_id>/', views.venue_event_list,
         name='events.venues.events_list'),

]
