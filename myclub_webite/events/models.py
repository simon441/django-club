from django.db import models
from django.contrib.auth.models import User

from datetime import date

# Create your models here.


class Venue(models.Model):
    name = models.CharField('Venue Name', max_length=120)
    address = models.CharField(max_length=300)
    zip_code = models.CharField('Zip Code', max_length=15)
    phone = models.CharField('Contact Phone', max_length=25, blank=True)
    web = models.URLField('Website Address', blank=True)
    email_address = models.EmailField('Email Address', blank=True)
    owner = models.IntegerField('Venue Owner', blank=False, default=1)
    venue_image = models.ImageField(null=True, blank=True, upload_to='images/')

    def __str__(self):
        """Unicode representation of Venue."""
        return self.name


class MyClubUser(models.Model):
    first_name = models.CharField('First Name', max_length=30)
    last_name = models.CharField('Last Name', max_length=30)
    email = models.EmailField('User Email')

    def __str__(self):
        return self.first_name + " " + self.last_name


class Event(models.Model):
    """Model definition for Event."""
    name = models.CharField('Event name', max_length=120)
    event_date = models.DateTimeField('Event Date')
    venue = models.ForeignKey(to=Venue, blank=True,
                              null=True, on_delete=models.CASCADE)
    # manager = models.CharField(max_length=60)
    manager = models.ForeignKey(
        User, blank=True, null=True, on_delete=models.SET_NULL)
    description = models.TextField(blank=True)

    attendees = models.ManyToManyField(to=MyClubUser, blank=True)

    approved = models.BooleanField('Approved', default=False)  # approved event

    class Meta:
        """Meta definition for Event."""

        verbose_name = 'Event'
        verbose_name_plural = 'Events'

    def __str__(self):
        """Unicode representation of Event."""
        return self.name

    @property
    def Days_ago(self) -> str:
        today = date.today()
        date_until: date = self.event_date.date() - today
        days_until_stripped = str(date_until).split(',', 1)[0].strip()
        return days_until_stripped

    @property
    def Is_Past(self) -> bool:
        today = date.today()
        return self.event_date.date() < today

    @property
    def Is_Past_Event_Text(self) -> str:
        return "Past" if self.Is_Past else "Future"
