from django import forms
from django.forms import ModelForm

from .models import Event, Venue


class VenueForm(forms.ModelForm):
    """Form definition for Venue."""

    class Meta:
        """Meta definition for Venueform."""

        model = Venue
        fields = ('name', 'address', 'zip_code',
                  'phone', 'web', 'email_address', 'venue_image', )

        # labels = {
        #     'name': '',  # 'Enter your Venue here',
        #     'address': '',
        #     'zip_code': '',
        #     'phone': '',
        #     'web': '',
        #     'email_address': '',
        # }

        widgets = {
            'name': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Venue Name'}),
            'address': forms.Textarea(attrs={'class': 'form-control', 'placeholder': 'Address'}),
            'zip_code': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Zip Code'}),
            'phone': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Phone Number'}),
            'web': forms.URLInput(attrs={'class': 'form-control', 'placeholder': 'Website address'}),
            'email_address': forms.EmailInput(attrs={'class': 'form-control', 'placeholder': 'Email'}),
        }


class EventFormAdmin(forms.ModelForm):
    """Admin SuperUser Form definition for Event."""

    class Meta:
        """Meta definition for Eventform."""

        model = Event
        fields = ('name', 'event_date', 'venue',
                  'manager', 'attendees', 'description', )

        labels = {
            'name': 'Name',
            'event_date': 'Event Date (format: YYYY-MM-DD HH:MM:SS)',
            'venue': 'Venue',
            'manager': 'Manager',
            'attendees': 'Attendees',
            'description': 'Description',
        }

        widgets = {
            'name': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Event Name'}),
            'event_date': forms.DateTimeInput(attrs={'class': 'form-control', 'placeholder': 'Event Date', 'type': 'datetime-local', }),
            # 'event_date': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Event Date', 'pattern': '^\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2}$'}),
            'venue': forms.Select(attrs={'class': 'form-select', 'placeholder': 'Venue'}),
            'manager': forms.Select(attrs={'class': 'form-select', 'placeholder': 'Manager'}),
            'attendees': forms.SelectMultiple(attrs={'class': 'form-control', 'placeholder': 'Attendees'}),
            'description': forms.Textarea(attrs={'class': 'form-control', 'placeholder': 'Description'}),
        }


class EventForm(forms.ModelForm):
    """User Form definition for Event."""

    class Meta:
        """Meta definition for Eventform."""

        model = Event
        fields = ('name', 'event_date', 'venue', 'attendees', 'description', )

        labels = {
            'name': 'Nama',
            'event_date': 'Event Date (format: YYYY-MM-DD HH:MM:SS)',
            'venue': 'Venue',
            'attendees': 'Attendees',
            'description': 'Description',
        }

        widgets = {
            'name': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Event Name'}),
            'event_date': forms.DateTimeInput(attrs={'class': 'form-control2', 'placeholder': 'Event Date', 'pattern': '^\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2}$'}),
            # 'event_date': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Event Date', 'pattern': '^\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2}$'}),
            'venue': forms.Select(attrs={'class': 'form-select', 'placeholder': 'Venue'}),
            'attendees': forms.SelectMultiple(attrs={'class': 'form-control', 'placeholder': 'Attendees'}),
            'description': forms.Textarea(attrs={'class': 'form-control', 'placeholder': 'Description'}),
        }
