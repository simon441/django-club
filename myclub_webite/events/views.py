import calendar
import csv
import io
from calendar import HTMLCalendar
from datetime import datetime
from typing import List, Union

from django.contrib import messages
# Import User Model from Django
from django.contrib.auth.models import User
# Import pagination stuff
from django.core.paginator import Paginator
from django.db.models import Prefetch
from django.db.models.query import QuerySet
from django.db.models.query_utils import Q
from django.http import (FileResponse, HttpRequest, HttpResponse,
                         HttpResponseRedirect, Http404)
from django.shortcuts import get_object_or_404, redirect, render
# reportlab imports
from reportlab.lib.pagesizes import letter
from reportlab.lib.units import inch
from reportlab.pdfgen import canvas

from .forms import EventForm, EventFormAdmin, VenueForm
from .models import Event, Venue

# Create your views here.


def _get_current_date() -> List[Union[int, str]]:
    # Get current year
    now = datetime.now()
    current_year = now.year

    # Get current time
    time = now.strftime('%H:%M')
    return [current_year, time]


def home(request: HttpRequest, year: int = datetime.now().year, month: str = datetime.now().strftime('%B')):
    """ Home Page """
    # Guest is user is not logged in (empty string is considred false)
    # see https://docs.python.org/3/library/stdtypes.html#textseq
    name = request.user.username or 'Guest'

    month = month.capitalize()
    # Convert month to number
    month_number = list(calendar.month_name).index(month)
    month_number = int(month_number)

    # Create a calendar
    cal = HTMLCalendar().formatmonth(year, month_number)

    # # Get current year
    # now = datetime.now()
    # current_year = now.year

    # # Get current time
    # time = now.strftime('%H:%M')
    [current_year, time] = _get_current_date()

    # Query the Event Model
    event_list = list(
        Event.objects.filter(event_date__year=year,
                                      event_date__month=month_number)
                    .prefetch_related('attendees', 'manager', 'venue')
        )

    return render(request, 'events/home.html', {
        'name': name,
        'year': year,
        'month': month,
        'cal': cal,
        'current_year': current_year,
        'time': time,
        'event_list': event_list,
    })


def all_events(request: HttpRequest) -> HttpResponse:
    """ Display All Events """

    # to order: use order_by. type in the field names in quotes, comma-separated.
    # Put a minus sign '-' to order by DESCending order
    # To order randomly, use "?"
    # event_list = Event.objects.all()
    event_list = list(Event.objects.order_by('-event_date').prefetch_related('attendees','manager', 'venue'))
    [current_year, time] = _get_current_date()
    return render(request=request, template_name='events/event_list.html', context={'event_list': event_list, 'current_year': current_year})


def show_event(request: HttpRequest, event_id: int) -> HttpResponse:
    """Show an Event

    Args:
        request (HttpRequest)
        event_id (int)

    Returns:
        HttpResponse
    """
    event: Event = get_object_or_404(Event, pk=event_id)
    if not event.approved:
        messages.warning(request, """This event hasn't been approved yet""")
    return render(request, 'events/event_show.html', {'event': event})


def add_venue(request: HttpRequest) -> HttpResponse:
    """ Add a Venue """
    submitted = False
    if request.method == 'POST':
        form = VenueForm(request.POST, request.FILES)
        if form.is_valid():
            venue = form.save(commit=False)
            venue.owner = request.user.id  # logged in user
            venue.save()
            # form.save()
            return HttpResponseRedirect('/add_venue?submitted=true')
    else:
        form = VenueForm()
        if 'submitted' in request.GET:
            submitted = True
    return render(request, template_name='events/add_venue.html', context={'form': form, 'submitted': submitted})


def list_venues(request: HttpRequest) -> HttpResponse:
    """ list all venues """

    # venue_list = Venue.objects.all().order_by('?')
    venue_list = Venue.objects.all().order_by('id')

    # set up pagination
    p = Paginator(venue_list, 3)
    page = request.GET.get('page')

    venues = p.get_page(page)

    return render(request, 'events/venues.html', context={'venues': venues})


def show_venue(request: HttpRequest, venue_id: int) -> HttpResponse:
    """ Show a Venue """
    venue: Venue = Venue.objects.get(pk=venue_id)
    venue_owner: User = User.objects.get(pk=venue.owner)
    return render(request, 'events/venue_show.html', {'venue': venue, 'venue_owner': venue_owner})


def search_venues(request: HttpRequest) -> HttpResponse:
    """ Search venues """
    if request.method == 'POST':
        query = request.POST.get('q', '')
        venues = Venue.objects.filter(name__contains=query)

        return render(request,
                      'events/search_venues.html',
                      context={'q': query, 'venues': venues}
                      )
    else:
        return render(request, 'events/search_venues.html', context={})


def update_venue(request: HttpRequest, venue_id) -> HttpResponse:
    """ Update a Venue """
    venue = Venue.objects.get(pk=venue_id)
    form = VenueForm(request.POST or None,
                     request.FILES or None, instance=venue)
    if form.is_valid():
        form.save()
        return redirect('events.venues.list')
    return render(request, 'events/update_venue.html', {
        'venue': venue,
        'form': form
    }
    )


def delete_venue(request: HttpRequest, venue_id: int) -> HttpResponse:
    if request.method == 'POST':
        venue = Venue.objects.get(pk=venue_id)
        venue.delete()
    # else:
        # return HttpResponseNotAllowed(permitted_methods=['POST'])
    return redirect('events.venues.list')


def venue_text(request: HttpRequest) -> HttpResponse:
    """ Create and send text file """
    response = HttpResponse(content_type='text/plain')
    response['Content-Disposition'] = 'attachement; filename=venues.txt'

    # Designate the Model
    venues = Venue.objects.all()

    # Create empty list
    lines = []
    # Loop through and output
    for venue in venues:
        line = f'{venue.name}\n{venue.address}\n{venue.zip_code}\n'
        if venue.phone != '':
            line += f'{venue.phone}\n'
        if venue.web != '':
            line += f'{venue.web}\n'
        if venue.email_address != '':
            line += f'{venue.email_address}\n'
        line += '\n\n'
        # \n{venue.web}\n{venue.email_address}\n\n\n
        lines.append(line)

    # lines = ['This is line 1\n',
    #          'This is line 2\n',
    #          'This is line 3\n',
    #          ]

    #   Write to TexFile
    response.writelines(lines)
    return response


def venue_csv(request: HttpRequest) -> HttpResponse:
    """ Create and send CSV file """
    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachement; filename=venues.csv'

    # Create a CSV Writer
    writer = csv.writer(response)

    # Designate the Model
    venues = Venue.objects.all()

    # Add  Columns Headings to CSV File
    writer.writerow(['Venue Name', 'Address', 'Zip Code',
                     'Phone', 'Web Address', 'Email'])

    # Loop through and output
    for venue in venues:
        writer.writerow([venue.name, venue.address, venue.zip_code,
                         venue.phone, venue.web, venue.email_address])

    return response


def venue_pdf(request: HttpRequest) -> FileResponse:
    """ Generate a PDF File Venue list """

    # Create ByteStream Buffer
    buffer = io.BytesIO()
    # Create a Canvas
    c = canvas.Canvas(buffer, pagesize=letter, bottomup=0)
    # Create a text object
    text_object = c.beginText()
    text_object.setTextOrigin(inch, inch)
    text_object.setFont('Helvetica', 14)

    # Add some lines of text
    # lines = [
    #     'This is line 1',
    #     'This is line 2',
    #     'This is line 3',
    # ]

    # Designate the Model
    venues = Venue.objects.all()

    # Create an empty list
    lines = []

    # Loop through the venues
    for venue in venues:
        lines.append(venue.name)
        lines.append(venue.address)
        lines.append(venue.zip_code)
        if venue.phone != '':
            lines.append(venue.phone)
        if venue.web != '':
            lines.append(venue.web)
        if venue.email_address != '':
            lines.append(venue.email_address)
        # lines.append(' ')
        # lines.append('-----------------------------------')
        lines.append('===================================')
        # lines.append(' ')

    # Loop
    for line in lines:
        text_object.textLine(line)

    # Finish Up
    c.drawText(text_object)
    c.showPage()
    c.save()
    buffer.seek(0)
    return FileResponse(buffer, as_attachment=True, filename='venue.pdf')


def add_event(request: HttpRequest) -> HttpResponse:
    """ Add an Event """
    submitted = False
    if request.method == 'POST':
        if request.user.is_superuser:
            form = EventFormAdmin(request.POST)
            if form.is_valid():
                form.save()
                return HttpResponseRedirect('/add_event?submitted=true')
        else:
            form = EventForm(request.POST)
            if form.is_valid():
                event = form.save(commit=False)
                event.manager = request.user  # logged in user
                event.save()
                return HttpResponseRedirect('/add_event?submitted=true')
    else:
        # Just going to the page, not submitting
        if request.user.is_superuser:
            form = EventFormAdmin()
        else:
            form = EventForm()
        if 'submitted' in request.GET:
            submitted = True
    return render(request, template_name='events/add_event.html', context={'form': form, 'submitted': submitted})


def update_event(request: HttpRequest, event_id: int) -> HttpResponse:
    """ Update an Event """

    event = Event.objects.get(pk=event_id)
    if request.user.is_superuser:
        form = EventFormAdmin(request.POST or None, instance=event)
    else:
        form = EventForm(request.POST or None, instance=event)

    if form.is_valid():
        form.save()
        return redirect('events.list')

    return render(request, template_name='events/update_event.html', context={'event': event, 'form': form})


def delete_event(request: HttpRequest, event_id: int) -> HttpResponse:
    """ Delete an Event """
    if request.method == 'POST':
        event = Event.objects.get(pk=event_id)
        if request.user == event.manager:
            messages.success(request, ('Event deleted!'))
            event.delete()
        else:
            messages.error(
                request, ('You are not authorized to delete this event'))
    else:
        messages.error(
            request, ('You are not authorized to delete this event'))
    return redirect('events.list')


def my_events(request: HttpRequest) -> HttpResponse:
    """My Events Page"""
    if request.user.is_authenticated:
        my_events = Event.objects.filter(attendees=request.user.id).prefetch_related('attendees', 'manager', 'venue')
        context = {'my_events': my_events}
        return render(request, 'events/my_events.html', context=context)
    else:
        messages.error(
            request, ('You are not authorized to view this page'))
    return redirect('events.home')


def search_events(request: HttpRequest) -> HttpResponse:
    """ Search events """
    query = request.POST.get('q', '').strip()
    if request.method == 'POST':
        events = Event.objects.filter(
            Q(name__icontains=query) | Q(description__icontains=query) | Q(venue__name__icontains=query))

        return render(request,
                      'events/search_events.html',
                      context={'q': query, 'events': events}
                      )
    else:
        return render(request, 'events/search_events.html', context={'q': query})


def venue_event_list(request: HttpRequest, venue_id: int) -> HttpResponse:
    """List all events for a given Venue

    Args:
        request (HttpRequest):
        venue_id (int): [Venue id]

    Returns:
        HttpResponse
    """
    # Get the venue
    venue: Venue = get_object_or_404(Venue, pk=venue_id)
    # Grab the Events from that Venue
    events: QuerySet[Event] = venue.event_set.all()

    if events:
        return render(request, 'events/venue_events_list.html', {
            'events': events,
        })

    messages.warning(request, ('That Venue has no events at this time...'))
    return redirect('events.admin_approval')


def admin_approval(request: HttpRequest) -> HttpResponse:
    """Admin Approval Page

    Args:
        request (HttpRequest):

    Returns:
        HttpResponse:
    """
    # Only superuser is allowed to add or remove event approval
    if not request.user.is_superuser:
        messages.error(request, ('Your are not authorized to view this page'))
        return redirect('events.home')

    # Get events
    event_list: QuerySet[Event] = Event.objects.all().order_by('-event_date')
    
    # Form is Posted
    if request.method == 'POST':
        # List of event ids to update
        events_id: List[str] = request.POST.getlist('boxes[]')

        # events_list_ids = list(Event.objects.all().values_list('pk'))
        # events_list_ids.sort()
        # print(events_list_ids)
        # for x in events_list_ids:
        #     if events_id.count(f'{x[0]}') == 1:
        #         Event.objects.filter(pk=x[0]).update(approved=True)
        # Uncheck all events
        event_list.update(approved=False)
        # Update the database
        # for x in events_id:
        #     Event.objects.filter(pk=int(x)).update(approved=True)
        Event.objects.filter(id__in=events_id).update(approved=True)
        # Update success
        messages.success(request, ('Event List Approval Has Been Updated!'))
        return redirect('events.list')

    # Get Venues
    venue_list: QuerySet[Venue] = Venue.objects.all()

    # Get counts (for each model)
    event_count = len(event_list)
    venue_count = Venue.objects.count()
    user_count = User.objects.count()

    return render(request=request, template_name='events/admin_approval.html', context={
        'event_list': event_list,
        'event_count': event_count,
        'user_count': user_count,
        'venue_count': venue_count,
        'venue_list': venue_list,
    })
