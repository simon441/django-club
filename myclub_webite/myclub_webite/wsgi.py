"""
WSGI config for myclub_webite project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/3.1/howto/deployment/wsgi/
"""

import os

from django.core.wsgi import get_wsgi_application
from waitress import serve
import sys


os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'myclub_webite.settings')

application = get_wsgi_application()

if __name__ == '__main__' and sys.argv.count('manage.py') == 0 or sys.argv.count('runserver') == 0:
    serve(app=application)
