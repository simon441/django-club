from django.shortcuts import render, redirect
from django.http import HttpRequest, HttpResponse
from django.contrib.auth import authenticate, login, logout
from django.contrib import messages
from .forms import RegistrationUserForm


def login_user(request: HttpRequest) -> HttpResponse:
    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(request, username=username, password=password)
        if user is not None:
            login(request, user)
            # Redirect to a success page
            return redirect('events.home')
        else:
            # Return an 'invalid login' error message
            # messages.error(request, 'The was an error loggin in, try again')
            messages.error(request, 'Invalid credentials. Please try again')
            return redirect('login')
    else:
        return render(request, 'authentication/login.html')


def logout_user(request: HttpRequest) -> HttpResponse:
    """ Log out user """
    logout(request)
    messages.success(request, 'You Were Logged Out!')
    return redirect('events.home')


def register_user(request: HttpRequest) -> HttpResponse:
    if request.method == 'POST':
        form = RegistrationUserForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            password = form.cleaned_data.get('password1')
            user = authenticate(request, username=username, password=password)
            login(request, user)
            messages.success(request, 'Registration successful!')
            return redirect('events.home')
    else:
        form = RegistrationUserForm()
    return render(request, 'authentication/register_user.html', {
        'form': form
    })
